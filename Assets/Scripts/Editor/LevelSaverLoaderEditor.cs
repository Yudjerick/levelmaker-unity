using System;
using SavingLoading;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(LevelSaverLoader))]
    public class LevelSaverLoaderEditor : UnityEditor.Editor
    {
        private SerializedProperty _mConfiguration;
        private void OnEnable()
        {
             _mConfiguration = serializedObject.FindProperty("configuration");
        }

        public override void OnInspectorGUI()
        {
            LevelSaverLoader levelSaverLoader = (LevelSaverLoader)target;
            levelSaverLoader.dataToLoad = EditorGUILayout.TextField("JSON", levelSaverLoader.dataToLoad);
            EditorGUILayout.PropertyField(_mConfiguration, new GUIContent("Configuration"));

            if (GUILayout.Button("Save"))
            {
                levelSaverLoader.Save();
            }
            
            if (GUILayout.Button("Load"))
            {
                levelSaverLoader.Load();
            }
            
            serializedObject.ApplyModifiedProperties();
        }
    }
}
