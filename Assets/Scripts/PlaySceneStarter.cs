using System.Collections;
using System.Collections.Generic;
using SavingLoading;
using UnityEngine;

public class PlaySceneStarter : MonoBehaviour
{
    [SerializeField] private string levelJson;
    [SerializeField] private ModeChangeController modeChange;

    [SerializeField] private LevelSaverLoader loader;
    void Start()
    {
        //TO DO: get random level with request
        
        loader.dataToLoad = levelJson;
        loader.Load();
        modeChange.ChangeMode();
    }
}
