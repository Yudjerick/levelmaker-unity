﻿using System;

namespace Network
{
    [Serializable]
    public class Response
    {
        public string message;
        public string token;
        public User user;
    }
}