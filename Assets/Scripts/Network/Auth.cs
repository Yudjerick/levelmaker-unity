using UnityEngine;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Network;
using TMPro;
using UnityEngine.SceneManagement;

public class Auth : MonoBehaviour
{
    [SerializeField] private ErrorMessage errorMessage;
    public static User authorized;
    [SerializeField] private TMP_InputField login;
    [SerializeField] private TMP_InputField password;
    static HttpClient httpClient = new HttpClient();
    void Start()
    {
        
    }
    void Update()
    {
        
    }

    public void Register()
    {
        print("{" + $"\"name\": \"{login.text}\", \"password\": \"{password.text}\"" +"}");
        RegAsync();
    }

    public void Login()
    {
        LoginAsync();
    }
    async Task RegAsync(){
        HttpContent content = new StringContent("{" + $"\"name\": \"{login.text}\", \"password\": \"{password.text}\"" +"}", 
            Encoding.UTF8, "application/json");
        using HttpResponseMessage responseMessage = await httpClient.PostAsync("http://45.12.239.174:5000/api/user/reg", content);
        string responseString = await responseMessage.Content.ReadAsStringAsync();
        Response response = JsonUtility.FromJson<Response>(responseString);
        if (response.message != null)
        {
            errorMessage.showToast(response.message, 2);
        }
        //print(responseString);
    }
    
    async Task LoginAsync(){
        HttpContent content = new StringContent("{" + $"\"name\": \"{login.text}\", \"password\": \"{password.text}\"" +"}", 
            Encoding.UTF8, "application/json");

        using HttpResponseMessage responseMessage =
            await httpClient.PostAsync("http://45.12.239.174:5000/api/user/login", content);
        string c = await responseMessage.Content.ReadAsStringAsync();
        print(c);
        Response response = JsonUtility.FromJson<Response>(c);
        if (response.message == null)
        {
            authorized = response.user;
            SceneManager.LoadScene("Menu");
        }
        if (response.message != null)
        {
           errorMessage.showToast(response.message, 2);
        }
    }
}
