﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using SavingLoading;
using UnityEngine;

namespace Network
{
    public class RandomLevenLoader: MonoBehaviour
    {
        [SerializeField] private LevelSaverLoader loader;
        static HttpClient httpClient = new HttpClient();
        private void Start()
        {
            LoadRandomLevel();

        }

        async Task LoadRandomLevel()
        {

            using HttpResponseMessage response =
                await httpClient.GetAsync($"http://45.12.239.174:5000/api/level/random?userId={Auth.authorized.id}");
            string responseString = await response.Content.ReadAsStringAsync();
            
            print(responseString);
            LevelResponse levelResponse = JsonUtility.FromJson<LevelResponse>(responseString);
            loader.dataToLoad = levelResponse.level.level_content.Replace('\'','"');
            loader.Load();
            GetComponent<ModeChangeController>().ChangeMode();
        }
    }
}