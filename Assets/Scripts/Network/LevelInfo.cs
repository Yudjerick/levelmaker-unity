﻿using System;

namespace Network
{
    [Serializable]
    public class LevelInfo
    {
        public int id;
        public string level_content;
        public User user;
    }
}