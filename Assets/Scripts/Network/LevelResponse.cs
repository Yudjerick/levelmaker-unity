﻿using System;
using UnityEngine;

namespace Network
{
    [Serializable]
    public class LevelResponse
    {
        public LevelInfo level;
    }
}