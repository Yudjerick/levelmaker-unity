﻿using System;

namespace Network
{
    [Serializable]
    public class User
    {
        public int id;
        public string name;
        public int gold;
        public int experience;
        public int score;
    }
}