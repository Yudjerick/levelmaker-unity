﻿
using SavingLoading;
using UnityEngine;

namespace DefaultNamespace
{
    public class Saw: LevelObject
    {
        public float speed;
        public bool enableRotation = true;
        private void Update()
        {
            if(enableRotation)
                transform.Rotate(new Vector3(0,0,speed));
        }
    }
}