using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unique : MonoBehaviour
{
    private string tag;
    void Start()
    {
        tag = gameObject.tag;
        foreach (var g in GameObject.FindGameObjectsWithTag(tag))
        {
            if (g.gameObject != gameObject)
            {
                Destroy(g);
            }
        }
    }
    
    void Update()
    {
        
    }
}
