using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    public float speed;
    public float jumpSpeed = 5f;
    public Transform groundCheck;
    public float groundRadius = 0.2f;
    public LayerMask groundLayer;
    
    private bool _isGrounded = false;
    private Vector3 _initialPosition;

    private Rigidbody2D _rb;
    void Start()
    {
        _initialPosition = transform.position;
        _rb = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        _rb.velocity = new Vector2(Input.GetAxis("Horizontal")*speed, _rb.velocity.y);


        _isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, groundLayer);

        if (_isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            _rb.velocity = new Vector2(_rb.velocity.x, jumpSpeed);
            _isGrounded = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Deadly"))
        {
            transform.position = _initialPosition;
        }
    }
}
