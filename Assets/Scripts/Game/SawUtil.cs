﻿using System;
using System.Collections.Generic;
using SavingLoading;
using UnityEngine;

namespace DefaultNamespace
{
    [RequireComponent(typeof(Saw))]
    public class SawUtil: BundleUtil
    {
        private Saw _saw;

        private void Awake()
        {
            _saw = gameObject.GetComponent<Saw>();
        }

        public override void Parse(Bundle bundle)
        {
            _saw.speed = bundle.floats[0];
        }

        public override Bundle CreateBundle()
        {
            List<float> floats = new List<float>();
            floats.Add(_saw.speed);
            Bundle bundle = new Bundle(floats);
            return bundle;
        }
    }
}