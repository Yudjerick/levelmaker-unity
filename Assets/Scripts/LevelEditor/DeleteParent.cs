using UnityEngine;

public class DeleteParent : MonoBehaviour
{
    private void OnMouseDown()
    {
        Destroy(transform.parent.gameObject);
    }
}
