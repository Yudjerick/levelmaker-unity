using System;
using System.Collections;
using System.Collections.Generic;
using LevelEditor;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Transform saver;
    private void OnMouseDown()
    {
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Instantiate(SelectionManager.SelectedItem, (Vector2)worldPosition, Quaternion.identity, saver);
    }
}
