using UnityEngine;

namespace LevelEditor
{
    public class ItemSelectButton : MonoBehaviour
    {
        [SerializeField] private GameObject associatedItem;
        [SerializeField] GameObject _hightlight;

        private void Start()
        {
            _hightlight = transform.GetChild(0).gameObject;
        }

        public void OnClick()
        {
            SelectionManager.SelectedItem = associatedItem;
            _hightlight.SetActive(true);
        }
    }
}
