using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class MenuButtons : MonoBehaviour
    {
        public void OpenEditor()
        {
            SceneManager.LoadScene("Editor");
        }

        public void OpenPlayScene()
        {
            SceneManager.LoadScene("PlayScene");
        }

        public void OpenMenu()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
