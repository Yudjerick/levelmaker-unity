﻿using System;
using System.Collections.Generic;

namespace SavingLoading
{
    [Serializable]
    public struct Bundle
    {
        public List<float> floats;

        public Bundle(List<float> floats)
        {
            this.floats = floats;
        }
    }
}