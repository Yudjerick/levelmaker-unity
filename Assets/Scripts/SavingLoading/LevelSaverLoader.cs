using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Unity.VisualScripting;
using UnityEditor.VersionControl;
using UnityEngine;
using Task = System.Threading.Tasks.Task;

namespace SavingLoading
{
    public class LevelSaverLoader : MonoBehaviour
    {
        static HttpClient httpClient = new HttpClient();
        public string dataToLoad;
        public LevelObjectConfiguration configuration;

        void Start()
        {
        }
    
        void Update()
        {
       
        }

        public void Save()
        {
            LevelData levelData = new LevelData();
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject saveTarget = transform.GetChild(i).gameObject;
                LevelObjectData levelObjectData = new LevelObjectData
                {
                    position = saveTarget.transform.position,
                    rotation = saveTarget.transform.rotation,
                    scale = saveTarget.transform.localScale,
                };
                levelObjectData.indexInConfiguration = FindInConfiguration(saveTarget);
            
                try
                {
                    levelObjectData.bundle = saveTarget.GetComponent<BundleUtil>().CreateBundle();
                }
                catch (Exception e)
                {
                
                }
            

                levelData.objects.Add(levelObjectData);
            }

            dataToLoad = JsonUtility.ToJson(levelData);
            dataToLoad = dataToLoad.Replace('"', '\'');
            SaveToDataBase(dataToLoad);
        }

        async Task SaveToDataBase(String lvlJson)
        {
            print("{" + $"\"userId\": {Auth.authorized.id}, \"level_address\": {lvlJson}"+"}");
            HttpContent content = new StringContent("{" + $"\"userId\": \"{Auth.authorized.id}\", \"level_address\": \"{lvlJson}\""+"}", 
                Encoding.UTF8, "application/json");

            using HttpResponseMessage response =
                await httpClient.PostAsync("http://45.12.239.174:5000/api/level/create", content);
            string c = await response.Content.ReadAsStringAsync();
            print(c);
        }

        public void Load()
        {
            LevelData levelData = JsonUtility.FromJson<LevelData>(dataToLoad);
            foreach (var levelObjectData in levelData.objects)
            {
                GameObject instantiate = Instantiate(configuration.objects[levelObjectData.indexInConfiguration], transform);
                instantiate.transform.SetLocalPositionAndRotation(levelObjectData.position, levelObjectData.rotation);
                instantiate.transform.localScale = levelObjectData.scale;


                try
                {
                    instantiate.GetComponent<BundleUtil>().Parse(levelObjectData.bundle);
                }
                catch (Exception e)
                {
                
                }

            }
        }

        int FindInConfiguration(GameObject instance)
        {
            for (int i = 0; i < configuration.objects.Count; i++)
            {
                if (instance.GetComponent<LevelObject>().configurationId == 
                    configuration.objects[i].GetComponent<LevelObject>().configurationId)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
