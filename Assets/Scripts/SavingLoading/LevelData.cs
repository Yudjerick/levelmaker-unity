﻿using System;
using System.Collections.Generic;
using DefaultNamespace;

namespace SavingLoading
{
    [Serializable]
    public class LevelData
    {
        public List<LevelObjectData> objects;

        public LevelData()
        {
            objects = new List<LevelObjectData>();
        }
    }
}