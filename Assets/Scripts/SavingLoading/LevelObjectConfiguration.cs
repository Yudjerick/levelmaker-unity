﻿using System.Collections.Generic;
using UnityEngine;

namespace SavingLoading
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/LevelObjectConfiguration", order = 1)]
    public class LevelObjectConfiguration: ScriptableObject
    {
        public List<GameObject> objects;
    }
}