﻿using System;
using UnityEngine;

namespace SavingLoading
{
    [Serializable]
    public struct LevelObjectData
    {
        public int indexInConfiguration;
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;
        public Bundle bundle;
    }
}