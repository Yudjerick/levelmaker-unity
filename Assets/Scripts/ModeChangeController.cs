using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeChangeController : MonoBehaviour
{
    public bool playModeEnabled;
    [SerializeField] private GameObject playerRef;

    public void ChangeMode()
    {
        playModeEnabled = !playModeEnabled;
        if (playModeEnabled)
        {
            Instantiate(playerRef, GameObject.FindGameObjectWithTag("Start").transform.position, Quaternion.identity);
        }
        else
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
        }
    }
}
